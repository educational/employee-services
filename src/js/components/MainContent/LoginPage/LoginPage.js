import React, {useState} from "react";
import s from "./LoginPage.module.css";
import cn from 'classnames';
import {SvgResetIcon} from './svgComponents/svgComponents';
import {InputWrap} from '../../../../assets/InputWrap/InputWrap';
import {NavLink} from "react-router-dom";
import { useSelector } from 'react-redux'
import { Redirect } from "react-router-dom";



function LoginPage (props) {
  
  const [usersLogin, setUsersLogin] = useState("")
  const [usersPassword, setUsersPassword] = useState("")
  const [redirect, setRedirect] = useState(null)
  const users = useSelector(state => state.users.users)
  
  
 
  const checkInputData=()=>{
    let coincidence = users.filter(function(el) {
      return el.name === usersLogin
    })

    if(coincidence.length < 1){
      alert('Incorrect login or password')
    } else if(coincidence[0].password !== usersPassword){
      alert('Incorrect password')
    } else{
      props.authorization(usersLogin, usersPassword);
      setRedirect('/')
    }
  }


 

  const handleInputLoginChange = (event) =>{
    setUsersLogin(event)
  }
  const handleInputPasswordChange = (event) =>{
    setUsersPassword(event)
  }

  let ref = React.createRef();
 

  if (redirect) {
    return <Redirect to={redirect} />
  }
  return (
    <div className={cn(s.content, s.loginScreen)} id="login-screen">
      <div className={s.wrapperForLogin}>
        <div className={s.loginHeaderWrap} >
          <div className={s.loginHeader}>Sign in!</div>
         
          <NavLink className={s.resetBtnLogin} to="/">
          <SvgResetIcon />
          </NavLink>
         
        </div>
        <div className={s.loginMain}>
          <div className={s.loginArea}>
            <form className={s.loginForm}>
              <div className={s.field}>
                <InputWrap label={"Username"} type={"text"} value={usersLogin} onChange={handleInputLoginChange} ref={ref} />
              </div>
              <div className={s.field}>
                <InputWrap label={"Password"} type={"password"} value={usersPassword} onChange={handleInputPasswordChange} ref={ref} />
              </div>        
            </form>
          </div>
          <button id="button-login" className={s.recaptchaTriggerButton} onClick={checkInputData}>Sign in</button>            
        </div>      
      </div>
    </div>
  );
  
};
export default LoginPage;
