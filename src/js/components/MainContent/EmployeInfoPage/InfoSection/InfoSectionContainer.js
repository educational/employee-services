import {connect} from 'react-redux';
import InfoSection from './InfoSection';


let mapStateToProps = (state) => {
  if (state.users.authorizedUser.length !== 0) {
    return {
           status: state.users.authorizedUser[0].status,   
         }
  }
}



const InfoSectionContainer = connect(mapStateToProps)(InfoSection)


export default InfoSectionContainer;