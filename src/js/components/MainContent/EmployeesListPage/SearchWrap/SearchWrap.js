import React, {useState} from "react";
import s from "./SearchWrap.module.css";
import cn from 'classnames';
import {SvgSearchIcon, SvgResetIcon} from './svgComponents/svgComponents';
import {InputWrap} from '../../../../../assets/InputWrap/InputWrap';

function SearchWrap (props) {

  let [userName, setUserName] = useState("")
  let [inputError, setInputError] = useState("")
  let ref = React.createRef()

  const handleSubmit = (event) =>{event.preventDefault()}
  
  
  const resetSearch = () =>{
    props.resetSearch()
    setUserName('')
    setInputError('')
  }


  const search = () => {
    if (inputCheck(props.searchedEmployees) === true){
      props.search()
    } else{
      setInputError('error')
    }
  }

  const handleInputChange = (event) =>{
    setUserName(event)
    props.searchedName(event)
  }

  const inputCheck = (name)=>{
    if (name !==''){
    let inputName =  props.employees.filter(function(el) {
      return (
        el.name.toUpperCase().indexOf(name.toUpperCase()) + 1 === 1 ||
        el.nativeName.toUpperCase().indexOf(name.toUpperCase()) + 1 === 1
    )})
    return inputName.length > 0 ? true : false
      }
  }

 
  
  
  return (
    <div className={cn(s.searchWrap, )}>
        <div className={s.searchTypeWrap}>
          <div id="basic-search-btn" className={s.searchTypeElement +' '+s.searchTypeActive}>basic search</div>
          <div id="advanced-search-btn" className={s.searchTypeElement}>advanced search</div>
        </div>
        <form name="searchForm" className={s.searchForm} onSubmit={handleSubmit}>
          <div className={cn(s.inputWrap, s.settinsInputWrap)}>
            <SvgSearchIcon />
            <InputWrap className={cn(s.searchFormInput, [s[inputError]])}
             placeholder="John Smith / Джон Смит"  type={"text"} value={userName}
              onChange={handleInputChange} ref={ref} />
            <div id="reset-btn" className={s.resetBtn} onClick={resetSearch}>
            <SvgResetIcon />
            </div>
          </div>
          <button id="search-btn" className={s.searchFormBtn} onClick={search}>search</button>
        </form>
      </div>
  );
}
export default SearchWrap;
