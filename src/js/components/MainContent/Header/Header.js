import React, {useState} from "react";
import s from "./Header.module.css";
import cursor from '../../../../assets/images/cursor.svg';
import off from '../../../../assets/images/off.svg';
import cn from 'classnames';
import {NavLink} from "react-router-dom";


function Header (props) {

  const [activeButt, setActiveButt] = useState("")
  
  const clockOnMaim =()=>{setActiveButt("")}
  const clockOnSettings =()=>{setActiveButt("settings")}


  return (
    <header>
      <div className={cn(s.headerWrap, s.content)}>
        <div className={s.logo}>
          <p className={s.logoName}>leverx group</p>
          <p>employee services</p>
        </div>
        <div className={s.headerButtons}>
        {
          activeButt === 'settings' ? 
          <>
            <div className={s.headerButtonAddres} ><NavLink to="/" onClick={clockOnMaim}>Addres Book</NavLink></div>
            <div className={cn(s.headerButtonSettings, s.headerButtonActive)} >{(props.status === "admin")?<NavLink to="/settings" onClick={clockOnSettings}>Settings</NavLink>:"Settings"}</div>
          </>:
          <>
            <div className={cn(s.headerButtonAddres, s.headerButtonActive)} ><NavLink to="/" onClick={clockOnMaim}>Addres Book</NavLink></div>
            <div className={s.headerButtonSettings} >{(props.status === "admin")?<NavLink to="/settings" onClick={clockOnSettings}>Settings</NavLink>:"Settings"}</div>
          </>
        }
        </div>
        <div className={s.headerButtons}>
          <div className={s.loginName}>{props.name}</div>
          <button className={s.btnLogin}><NavLink to="/login">Sign in</NavLink></button>
          <button className={s.headerButton}><img src={cursor} alt="cursor"/></button>
          <button className={s.headerButton}><img src={off} alt="off"/></button>
        </div>
      </div>
    </header>
  );
}
export default Header;
