import React, {useEffect} from "react";
import MainContent from './MainContent';
import {connect} from "react-redux";
import {getUsersThunkCreator} from '../../redux/employeesReducer';
import {employeesSearch, search} from '../../redux/employeesReducer';

const MainContentContainer = (props) => {

    useEffect(() => {props.getUsersThunkCreator()}, []);
    return ( <>
        {(props.users.employees.length !== 0)  && 
        <MainContent users = {props.users.employees}
                    searchedName={props.employeesSearch}
                    searchedEmployees={props.users.searchedEmployees}
                    search={props.search}
                    resetSearch={props.getUsersThunkCreator}/>
        }
        </>
    );
};

let mapDispatchToProps = (dispatch) => {
    return { 
        employeesSearch: (searchedName)=>{
            dispatch(employeesSearch(searchedName))
        },
        getUsersThunkCreator: ()=>{
            dispatch(getUsersThunkCreator())
        },
        search:()=>{
            dispatch(search())
        }
   };
 };

let mapStateToProps = (state) => {
    return {
        users: state.employees,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MainContentContainer);