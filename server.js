
var express = require('express');
var app = express();


var fs = require("fs");
var content = fs.readFileSync("data.json");
content = JSON.parse(content);

app.use(function (req, res, next) {
res.header('Access-Control-Allow-Origin', '*');
res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, access-control-allow-origin');

next();
});

app.get('/', function (req, res) {
res.send(content);
});


app.listen(3001, function () {
console.log(content);
});